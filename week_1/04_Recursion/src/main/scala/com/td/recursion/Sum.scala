package com.td.recursion

object Sum {

  def closedForm(n: Long): Long = ???

  def recursive(n: Long): Long = ???

  def tailrecursive(n: Long): Long = ???

}
